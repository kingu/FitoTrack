package de.tadris.fitness.recording.event;

public class PressureChangeEvent {

    public final float pressure;

    public PressureChangeEvent(float pressure) {
        this.pressure = pressure;
    }
}
